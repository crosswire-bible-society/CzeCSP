MOD_DIR=modules/texts/ztext/czecsp
MODULES=$(MOD_DIR)/nt $(MOD_DIR)/nt.vss $(MOD_DIR)/ot $(MOD_DIR)/ot.vss
#OSIS2MOD=/home/matej/archiv/knihovna/repos/sword/utilities/osis2mod
OSIS2MOD=osis2mod

all: test module

$(MODULES): bible.xml
	mkdir -p $(MOD_DIR)
	$(OSIS2MOD) $(MOD_DIR)  $< -v German -z -d 2

bible.xml: files.xml zdroje/*_*.xml CEPtoOSIS.py
	time ./CEPtoOSIS.py files.xml|cat -s>$@

module: $(MODULES) czecsp.conf
	zip -9qT CzeCSP.zip $(MOD_DIR)/* mods.d/*

zdroje/*_*.xml: original/*_*.xml
	rm zdroje/*.xml
	cp original/*.xml zdroje/

test: bible.xml
	xmllint --noout --schema podklady-OSIS/osisCore.2.1.1.xsd bible.xml

clean:
	rm -f *~ *out.xml bible.xml $(MODULES)
