#!/usr/bin/python3
import collections
import enum
import logging
import sys
import textwrap
import xml.sax
from xml.sax.saxutils import XMLFilterBase, XMLGenerator
from typing import Dict, List, Tuple
logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.DEBUG)
log = logging.getLogger('CEPtoOSIS')

Collected_head = collections.namedtuple('Collected_head', 'name attrs')
Event = collections.namedtuple('Event', 'type name content attrs')
EType = enum.Enum('EType', 'startElement endElement characters')
Morpho = collections.namedtuple('Morpho', 'lemma morph')

# error recovery
missing_notes = []

TESTAMENTS = {'ot': 'Stará smlouva',
              'nt': 'Nová smlouva'}

TranslDict = {
    "11_Gn.xml": "Gen",
    "12_Ex.xml": "Exod",
    "13_Lv.xml": "Lev",
    "14_Nu.xml": "Num",
    "15_Dt.xml": "Deut",
    "16_Joz.xml": "Josh",
    "17_Sd.xml": "Judg",
    "18_Rt.xml": "Ruth",
    "19_1S.xml": "1Sam",
    "20_2S.xml": "2Sam",
    "21_1Kr.xml": "1Kgs",
    "22_2Kr.xml": "2Kgs",
    "23_1Pa.xml": "1Chr",
    "24_2Pa.xml": "2Chr",
    "25_Ezd.xml": "Ezra",
    "26_Neh.xml": "Neh",
    "27_Est.xml": "Esth",
    "28_Jb.xml": "Job",
    "29_Psa.xml": "Ps",
    "30_Pro.xml": "Prov",
    "31_Kaz.xml": "Eccl",
    "32_Pis.xml": "Song",
    "33_Iz.xml": "Isa",
    "34_Jr.xml": "Jer",
    "35_Pl.xml": "Lam",
    "36_Ez.xml": "Ezek",
    "37_Da.xml": "Dan",
    "38_Oz.xml": "Hos",
    "39_Jl.xml": "Joel",
    "40_Am.xml": "Amos",
    "41_Abd.xml": "Obad",
    "42_Jon.xml": "Jonah",
    "43_Mi.xml": "Mic",
    "44_Na.xml": "Nah",
    "45_Abk.xml": "Hab",
    "46_Sf.xml": "Zeph",
    "47_Ag.xml": "Hag",
    "48_Za.xml": "Zech",
    "49_Mal.xml": "Mal",
    "61_Mt.xml": "Matt",
    "62_Mk.xml": "Mark",
    "63_Lk.xml": "Luke",
    "64_Jn.xml": "John",
    "65_Sk.xml": "Acts",
    "66_Ro.xml": "Rom",
    "67_1K.xml": "1Cor",
    "68_2K.xml": "2Cor",
    "69_Ga.xml": "Gal",
    "70_Ef.xml": "Eph",
    "71_Fp.xml": "Phil",
    "72_Ko.xml": "Col",
    "73_1Te.xml": "1Thess",
    "74_2Te.xml": "2Thess",
    "75_1Tm.xml": "1Tim",
    "76_2Tm.xml": "2Tim",
    "77_Tt.xml": "Titus",
    "78_Fm.xml": "Phlm",
    "79_He.xml": "Heb",
    "80_Jk.xml": "Jas",
    "81_1Pt.xml": "1Pet",
    "82_2Pt.xml": "2Pet",
    "83_1J.xml": "1John",
    "84_2J.xml": "2John",
    "85_3J.xml": "3John",
    "86_Ju.xml": "Jude",
    "87_Zj.xml": "Rev"}

# Error codes
ErrCode = enum.Enum('ErrCode', [('MISSING_NOTES', 13)])

TRANS_ELEM2CHAR = {
    'bczuv': '„', 'eczuv': '“',  # jiný druh citátů ??? např. Jan 8:10
    'pomlcka': '—', 'krat': '×', 'minus': '–',
    # takto vyznačujeme slova nebo pasáže, které jsou vypuštěny
    # pouze v méně významných skupinách rukopisů.
    'hzavorka': '[',
    # Viz. Jan 8:10 ... [<italic>text</italic>] je uděláno bez elementů

    # vymezení rozsahu odkazu na poznámku, kdy se poznámka vztahuje
    # k celému takto označenému textu, ne jenom k jednomu bodu.
    # OSIS XML nemá prostředky jak toto zaznamenat.
    "bkzavorka": '', "ekzavorka": '',

    "dots": '…', "amacron": 'ā', "omacron": 'ō',

    # used for example in note t9 on page 15 for Gn 4:3
    "hdotbelow": 'ḥ',

    # TODO these are not correct, they are subscript a and e, not
    # superscript used e.g., for elohim, note v2 on page. 11.
    "esuperior": 'ₑ', "asuperior": 'ₐ',

    "quoteleft": '‘', "quoteright": '’',

    "acircumflex": 'â', "bmacronbelow": 'ḇ', "dmacronbelow ": 'ḏ',
    "ecircumflex": 'ê', "emacron": 'ē', "gmacron": 'ḡ', "icircumflex": 'î',
    "kmacronbelow": 'ḵ', "sacute": 'ś', "sdotbelow": 'ṣ', "tdotbelow": 'ṭ',
    "tmacronbelow": 'ṯ', "ocircumflex": 'ô', "ucircumflex": 'û',

    # TODO that's zero, not o
    "osuperior": '⁰',

    # TODO  that's p-dot not p-macron
    "pmacron": 'ṗ',

    # chained reference; mark behind references; showing always
    # previous and following occurence of the expression or idiom
    "retez": '*',

    # large differences among various translations
    "rozdily": '$',

    # majority; TODO maybe just write full word? Would it matter?
    "vetsina": '&amp;',

    # úplný výčet odkazů ve StS, resp. NS, kde je patřičný
    # ekvivalent použit
    "final": '†',

    # volněji
    "tilde": '\u00A0',

    # označuje paralelní místo (shodné nebo téměř shodné)
    "shoda": '//',

    "spojovnik": '–',

    # mark in the list of references, marking the first occurence of
    # the (Hebrew) expression in the Old Testament
    'prvni': '@'
}

W_ELEMENTS = {
    # Greek verb is in perfectum (which expresses an action which
    # happened in the past, but which consequences are still visible
    # in the present time; Mt 1:22)
    'perf': Morpho(None, 'robinson:V-2RAI-3S'),

    # Future tense where both perfective and imperfective aspects
    # are possible. This marks majority of cases where both aspects
    # are possible. (Mt 1:21)
    'fut': Morpho(None, 'robinson:V-FAI-3S'),

    # historical present tens of the Greek verb, if it has been
    # translated into Czech as a past tense (e.g.:
    # <hvezda>řekl</hvezda> = ř.:říká).  If there is in the Greek
    # text participium of the present tense attached to the main
    # verb, then the mark is only on the main verb (Mt 2:13)
    'hvezda': Morpho(None, 'robinson:V-PEI-3S'),

    # marks a definite article (not available in Czech) for words
    # Bůh (God), duch (spirit), slovo (word) and pravda (truth)
    # (e.g., Mt 1:23)
    'clen': Morpho('strong:G3588', 'robinson:T-DSM'),

    # slovo je překladem ř. soma nebo jeho odvozenin (Mt 5:29)
    'soma': Morpho('strong:G4983', None),

    # slovo je překladem ř. sarx nebo jeho odvozenin (Mt 16:17)
    'sarx': Morpho('strong:G4561', None),

    # slovo je překladem ř. rhéma (Mt 4:4)
    'rhema': Morpho('strong:G4487', None)
    }

INDEX_OT = {
    '1': 'sg., jednotné číslo (singulár)',
    '2': 'du., dvojné číslo (duál)',
    '3': 'pl., množné číslo (plurál)',
    '4': 'm., mužský rod',
    '5': 'f., ženský rod',
    '6': 'pro femininní podmět užito slovesa v maskulinu',
    '7': 'podstatné jméno',
    '8': '''označení země či lidu; př. Egypt = Eypťané (1S 10,18);
            Amálek = Amálekovci (1S 14,48);
            v H. bráno jako kolektivium = země či město
            je “matkou” jeho obyvatel.''',
    '9': '''jiný tvar osobního nebo místního jména,
            než uvádí český překlad [srv. pozn k Jr 21,2; 27,1.6]''',
    '10': 'neurčeno (chybí determinant nebo výslovně vazbou s l°-)',
    '11': 'určeno (členem, zájmenným suffixem, vazbou)',
    '12': 'apozice (prosté přiřazení bez gramatické závislosti)',
    '13': 'casus pendens, syntakticky vyčleněný podmět (resp. přísl. urč.)',
    '14': 'samostatné zájmeno',
    '15': '''jméno nebo sloveso má navíc zájmenný sufix
             (svůj, jeho, … / sobě, jemu, …)''',
    '16': '''infinitiv absolutní [použito často při překladu příslovcem,
             např. 1S 1,10; Jr 22,10 („pláčem plačte“)]''',
    '17': 'infinitiv konstruktivní (vázaný)',
    '18': '''participium činné [použito často při překladu jménem;
             např. Jr 30,21]''',
    '19': 'participium trpné',
    '20': '''perfektum (faktuál, jakýsi „faktický slovesný čas“)
             [použito často při překladu tzv. prorockého perfektu futurem;
             Jr 13,17-18]''',
    '21': '''imperfektum (aktuál, cosi jako „aktualizující slovesný čas“);
             [označován zvláště imperativ tvořený v h. zvláště
             imperfektem (Jr 1,7.21)]''',
    '22': 'imperativ',
    '23': 'imperativ zesílený suffixem -á (enfatický imperativ)',
    '24': 'imperativ zesílený částicí ná [Jr 5,21]',
    '25': 'kohorativ',
    '26': 'jussiv (vyjádřen jako apokopát)',
    '27': '''hapax legomenon, jediný výskyt v StS (často nejistý význam)
             [není uvedeno u všech]''',
    '28': 'tiqqúné sóferím, písařské opravy [např. Jr 2,11]',
    '29': '''masora připojuje toto slovo k následujícímu pomocí
             přízvuku zvaného pojič''',
    '30': '''masora odděluje toto slovo od následujícímu pomocí přízvuku
             zvaného dělič''',
    '31': '''nejednoznačná reference (t.j. není zcela jasné,
             která osoba je míněna)''',
    '32': 'řečnická otázka (která předpokládá přitakání)',
}

HEADER_DATA = '''<osis xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.bibletechnologies.net/2003/OSIS/namespace https://crosswire.org/osis/osisCore.2.1.1.xsd">
    <osisText osisIDWork="CzeCSP" osisRefWork="bible" xml:lang="cs"
      canonical="true">
    <header>
      <revisionDesc resp="Matěj Cepl">
        <date>2010.09.07</date>
        <p>This is just an information about the book.</p>
      </revisionDesc>
      <work osisWork="CzeCSP">
        <title>Český studijní překlad Bible</title>
        <creator>Nadační fond překladu Bible</creator>
        <date event="eversion" type="Gregorian">2012-03-06</date>
        <publisher>Nadační fond překladu Bible</publisher>
        <type type="OSIS">Bible</type>
        <identifier type="OSIS">Bible.cs.CSP</identifier>
        <source>http://www.biblecsp.cz/</source>
        <language type="SIL">CES</language>
        <coverage>Czech 2010</coverage>
        <rights type="x-copyright">Copyright 2009 Nakladatelství KMS</rights>
        <rights type="x-license">CC BY-NC-ND 3.0 CZ</rights>
        <rights type="x-license-url">http://creativecommons.org/licenses/by-nc-nd/3.0/cz/</rights>
        <rights type="x-comments-to">Email comments to office in domain biblescp.cz</rights>
        <refSystem>Bible.MT</refSystem>
      </work>
      <work osisWork="strong">
        <refSystem>Dict.Strongs</refSystem>
      </work>
      <work osisWork="robinson">
        <refSystem>Dict.Robinson</refSystem>
      </work>
    </header>
    </osisText>
    </osis>'''


class CopyHandler(xml.sax.handler.ContentHandler):
    """
    Simple SAX handler just for copying string to SAX events.
    """
    def __init__(self, downstream):
        super().__init__()
        self.ds = downstream

    def startElement(self, name, attrs):
        self.ds.startElement(name, attrs)

    def endElement(self, name):
        # We have to include end tags for <osisText> and <osis>,
        # otherwise xml.sax.parseString fails in expat, but we don't
        # want to include them in our resulting XML document.
        if name not in ('osisText', 'osis'):
            self.ds.endElement(name)

    def characters(self, content):
        self.ds.characters(content)


class BaseProcessor(XMLFilterBase):
    """
    Root class for shared properties and methods.
    """
    def __init__(self, upstream, downstream):
        XMLFilterBase.__init__(self, upstream)
        self.ds = downstream
        self.lastVerse = 0  # never used in CEPProcessor

    def elem(self, name, content=None, attrs=None):
        if attrs is None:
            attrs = {}
        self.ds.startElement(name, attrs)
        if content is not None:
            self.ds.characters(content)
        self.ds.endElement(name)


class CEPProcessor(BaseProcessor):
    """
    Processor for files with lists of documents to be processed.
    """
    def __init__(self, upstream, downstream):
        super().__init__(upstream, downstream)
        self.curr_test = ''
        self.__book_file = None

    def startDocument(self):
        xml.sax.parseString(HEADER_DATA.encode('utf8'),
                            CopyHandler(self.ds))

    def endDocument(self):
        self.ds.endElement('osisText')
        self.ds.endElement('osis')

    def startElement(self, name, attrs):
        if name in ('ot', 'nt'):
            self.curr_test = name
            self.ds.startElement('div',
                                 {'type': 'bookGroup', 'canonical': 'true'})
            self.elem('title', TESTAMENTS[name])
        elif name == 'file':
            self.__book_file = ''

    def endElement(self, name):
        if name in ('ot', 'nt'):
            self.ds.endElement('div')
        elif name == 'file':
            BookProcessor(
                xml.sax.make_parser(), self.ds, self.__book_file).parse(
                    self.__book_file)
            self.__book_file = None

    def characters(self, content):
        if self.__book_file is not None:
            self.__book_file += content

class BookProcessor(BaseProcessor):
    """
    Processor of the biblical text itself.
    """
    def __init__(self, upstream, downstream, filename):
        super().__init__(upstream, downstream)
        self.refKniha = None
        self.bookName = TranslDict[filename.split('/')[1]]
        self.collectedHeader = []
        self.collected_note = []
        self.collected_title = None
        self.collecting_title = False
        # if not None -> we're collecting content of a note
        self.current_note = None
        self.lastChapter = 0
        self.lastVerse = 0
        self.notes = {}

    def create_event(self, event_type, name='', content='', attrs=None):
        if attrs is None:
            attrs = {}

        if self.current_note is not None:
            self.collected_note.append(
                Event(event_type, name, content, attrs))
        else:
            if event_type == EType.startElement:
                try:
                    self.ds.startElement(name, attrs)
                except:
                    log.debug('name = %s, attrs = %s', name, attrs)
                    raise
            elif event_type == EType.endElement:
                self.ds.endElement(name)
            elif event_type == EType.characters:
                self.ds.characters(content)

    def trans_change_start(self):
        if self.current_note is not None:
            self.create_event(EType.startElement, name='hi',
                              attrs={'type': 'italic'})
        else:
            self.create_event(EType.startElement, name='transChange',
                              attrs={'type': 'added'})

    def trans_change_end(self):
        if self.current_note is not None:
            self.create_event(EType.endElement, name='hi')
        else:
            self.create_event(EType.endElement, name='transChange')

    def elem(self, name, content=None, attrs=None):
        if attrs is None:
            attrs = {}
        self.create_event(EType.startElement, name=name, attrs=attrs)
        if content is not None:
            self.create_event(EType.characters,
                              content=content.replace('~', '\u00A0'))
        self.create_event(EType.endElement, name=name)

    def insert_title(self, title_type):
        if self.collected_title is not None:
            self.elem("title", self.collected_title,
                      attrs={'type': title_type})
            self.collected_title = None

    def getRefID(self):
        return "{}.{:d}.{:d}".format(self.refKniha, self.lastChapter,
                                     self.lastVerse)

    @staticmethod
    def indexOT(no):
        # This could be eventually extended to have a huge dict
        # translating codes into some human-readable explanation of what
        # each index means.
        if no in INDEX_OT:
            return textwrap.dedent(INDEX_OT[no]).replace('\n', ' ')
        else:
            return 'Pozn. {} v tabulce na str.\u00A01499'.format(no)

    def startElement(self, name, attrs):
        # FIXME onvert to z. str. 123 z podklady-OSIS
        # /OSIS2_1UserManual_06March2006_-_with_O'Donnell_edits.PDF,
        # appendix C "Normative Abbreviations for canonical and
        # deutero-canonical books
        if name == 'kniha':
            self.refKniha = self.bookName.replace('~', '\u00A0')
            log.info('Processing %s ...', self.refKniha)
            self.create_event(EType.startElement, name='div',
                              attrs={'type': 'book',
                                     'osisID': self.refKniha,
                                     'canonical': 'true'})
            # It seems individual Biblical books usually don't have
            # titles in OSIS

        elif name == 'titulek':
            # see https://wiki.crosswire.org/OSIS_pre-verse_titles
            # but I am not sure whether it matters
            self.collecting_title = True
            self.collected_title = ''

        elif name == 'kap':
            N = int(attrs['n'])
            ref_ID = '{}.{:d}'.format(self.refKniha, N)
            if self.refKniha is not None:
                refBase = '{}.{:d}'.format(self.refKniha, self.lastChapter)
                if N > 1:
                    self.elem('verse', attrs={'eID': '{}.{:d}'.format(
                        refBase, self.lastVerse)})
                    self.elem('chapter', attrs={'eID': refBase})

            self.elem('chapter', attrs={'sID': ref_ID, 'osisID': ref_ID})
            self.insert_title('chapter')
            self.lastChapter = N

        elif name == 'vers':
            curVerse = int(attrs['n'])
            refBase = '{}.{:d}'.format(self.refKniha, self.lastChapter)
            ref_ID = '{}.{:d}'.format(refBase, curVerse)
            # Find out whether this is a first verse in a chapter;
            # notice that <kap/> element is milestoned as well, so we
            # have to count a distance in <verse/> elements from it,
            # rather than use plain count()
            if curVerse != 1:
                self.elem('verse', attrs={'eID': '{}.{:d}'.format(
                    refBase, self.lastVerse)})
            self.elem('verse', attrs={'sID': ref_ID, 'osisID': ref_ID})
            self.insert_title('main')
            self.lastVerse = curVerse

        elif name == 'p':
            self.create_event(EType.startElement, name='p', attrs={})

        elif name in TRANS_ELEM2CHAR:
            self.create_event(EType.characters, content=TRANS_ELEM2CHAR[name])

        elif name == 'czap':
            self.create_event(EType.startElement, name='q',
                              attrs={'marker': "'"})

        # when the Greek expression needs to be present in multiple
        # words (except of grammatical particle "by" (for conditional),
        # zvratné pronoun "se", auxilliary verb "být" (to be) and
        # demonstrative (ukazovacích) pronouns), they are in majority
        # cases marked by the element <spoj>. E.g., Mt 3:17
        # <spoj>který</spoj> <spoj>říkal</spoj>, literally “which was
        # saying”
        #
        # Fow now we go by the path of the least resistance ... keep
        # <spoj> element intact and process it with a Python script.
        #
        # Actually, after discussion with the original translators we
        # came to the conclusion that the case of the spoj element is
        # hopeless, a way more complicated, and it should be just
        # ignored.
        elif name == 'spoj':
            pass

        # Add different morphological characteristics
        # also Strong’s numbers for some particular words
        elif name in W_ELEMENTS:
            el_w = W_ELEMENTS[name]
            el_w_attrs = {}
            if el_w.lemma is not None:
                el_w_attrs['lemma'] = el_w.lemma
            if el_w.morph is not None:
                el_w_attrs['morph'] = el_w.morph
            self.create_event(EType.startElement, name='w', attrs=el_w_attrs)

        # If the reference is in italic, it doesn't refer to the expression
        # where it is in the text, but rather to whole verse or its part.
        elif name in ('italic', 'italico'):
            self.trans_change_start()

        elif name in ('defpozn', 'defpozno'):
            self.current_note = attrs['n']

        elif name in ('odkaz', 'odkazo'):
            ELEM_TYPES = {'odkaz': 'study', 'odkazo': 'crossReference'}
            # Make sure we are not inside of the note, where
            # self.getRefID doesn't work
            assert self.current_note is None
            ref_ID = self.getRefID()

            N = attrs['n']
            elem_type = ELEM_TYPES[name]

            self.create_event(EType.startElement, name='note', attrs={
                'type': elem_type,
                'osisRef': ref_ID,
                'osisID': ref_ID + '!' + N,
                'n': N
            })
            try:
                for ev in self.notes[N]:
                    self.create_event(ev.type, ev.name, ev.content, ev.attrs)
            except KeyError:
                if self.refKniha == '1Chr':
                    log.error('N = {}'.format(N))
                    raise
                missing_notes.append('{} {}.{}: {}'.format(self.refKniha,
                                                           self.lastChapter,
                                                           self.lastVerse, N))
            self.create_event(EType.endElement, name='note')

        # references to the table 91 (in the Old Testament) of the
        # source notes.
        elif name == 'index':
            N = attrs['n']

            if self.current_note is not None:
                self.create_event(EType.characters, content='(')
                self.indexOT(no=N)
                self.create_event(EType.characters, content=')')
            else:
                ref_ID = self.getRefID()
                self.elem(name='note',
                          content=self.indexOT(no=N),
                          attrs={
                              'type': 'x-index',
                              'placement': 'inline',
                              'osisRef': ref_ID,
                              'osisID': ref_ID + '!i' + N,
                              'n': N
                          }
                         )

    def endElement(self, name):
        if name == 'kniha':
            refBase = '{}.{:d}'.format(self.refKniha, self.lastChapter)
            self.elem('verse', attrs={'eID': '{}.{:d}'.format(
                refBase, self.lastVerse)})
            self.elem('chapter', attrs={'eID': refBase})
            self.create_event(EType.endElement, name='div')

        elif name == 'p':
            self.create_event(EType.endElement, name='p')

        elif name == 'czap':
            self.create_event(EType.endElement, name='q')

        elif name == 'hzavorka':
            self.create_event(EType.characters, content=']')

        elif name in W_ELEMENTS:
            self.create_event(EType.endElement, name='w')

        elif name in ('italic', 'italico'):
            self.trans_change_end()

        elif name in ('defpozn', 'defpozno'):
            self.notes[self.current_note] = tuple(self.collected_note)
            self.current_note = None
            self.collected_note = []

        elif name == 'titulek':
            assert self.collected_title is not None, \
                "Empty element <titluek>"
            self.collecting_title = False

    def characters(self, content):
        content = content.replace('~', '\u00A0')
        if self.collecting_title:
            self.collected_title += content
        else:
            self.create_event(EType.characters, content=content)


if __name__ == "__main__":
    downstream_handler = XMLGenerator(encoding="utf-8",
                                      short_empty_elements=True)
    CEPProcessor(xml.sax.make_parser(),
                 downstream_handler).parse(sys.argv[1])
    if len(missing_notes) > 0:
        log.error('missing_notes = %s', missing_notes)
        sys.exit(ErrCode.MISSING_NOTES.value)
