#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals

import sys
import xml.sax

from xml.sax.saxutils import XMLGenerator


class ListTagsHandler(xml.sax.ContentHandler):
    def __init__(self):
        xml.sax.ContentHandler.__init__(self)
        self.elements = set()

    def startElement(self, name, attrs):  # noqa
        self.elements.add(name)

if __name__ == "__main__":
    downstream_handler = XMLGenerator(encoding="utf-8")
    parser = xml.sax.make_parser()
    handler = ListTagsHandler()
    parser.setContentHandler(handler)
    for bible_file in sys.argv[1:]:
        parser.parse(bible_file)
    dict_keys = sorted(handler.elements)
    print(dict_keys)
